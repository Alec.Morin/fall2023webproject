# A Guessing Colours Puzzle Game

## This is a game where the user will guess matching colour squares with the colour displayed, and then show their results.

This project is a colour guessing game that a user can play inside a browser. This is a simple fun project that is used to display skills in:

* HTML
* CSS
* Javascript
* JSON

To access this website use the this link to the website:
* https://sonic.dawsoncollege.qc.ca/~2139469/103/WebDev2/FinalProject/code/html/

Link to gitlab repo:
* https://gitlab.com/Alec.Morin/fall2023webproject

## Steps on playing the game

1. Enter your player name
2. Choose the table size you will be guessing from
3. Chosse the colour you wish to guess
4. Choose the difficulty of the game
5. If your name is valid, click the "Start Puzzle Game" button
6. Click on the boxes you believe are the current colour (Text will display how much boxes are of the guessing colour)
7. Once finished guessing, click the "Submit Your Guesses" button
8. Your results will be displayed on the left of the screen in a table. They can be sorted by clicking the table header
9. You can optionally choose to save your game sessions by clicking on the "Save Game Sessions" button
10. You can load previous sessions that have been previously saved into the table by clicking the "Load Game Sessions" button
11. You can also clear all saved game sessions by clicking the "Clear All" button
12. To start another game, choose your name again and restart the steps
