'use strict';

const nameOption = document.querySelector('#playerName');
const tableSizeOption = document.querySelector('#tableSizeOption');
const colourOptionDiv = document.querySelector('#colourOptionsInnerDiv');
const gameDifficulty = document.querySelector('#gameDifficulty');
const cheatsOptions = document.querySelector('#cheatOption');
const startPuzzleButton = document.querySelector('#startPuzzleButton');

/**
 * Pushes the gameSettings of the current session to the gameSessions array
 * @param totalSeconds elapsed time while playingh the game
 * @param totalClicks total clicks on the game board
 * @param successPercentage percentage of correct choices
 * @author David Parker
 */
function pushGameSettings(totalSeconds, totalClicks, successPercentage) {
    const gameSettings = {
        name: getElementValue('#playerName'),
        duration: totalSeconds,
        clicks: totalClicks,
        difficulty: getElementValue('#gameDifficulty'),
        success: successPercentage,
        size: getElementValue('#tableSizeOption'),
    };
    gameSessions.push(gameSettings);
}

/**
 * Event listener to validate name input
 * Enables button when valid, disables when invalid
 * @author David Parker
 */
nameOption.addEventListener('input', (event) => {
    const startButton = document.querySelector('#startPuzzleButton');
    const nameRegex = /^[a-zA-Z0-9]{4,12}$/;
    if (nameRegex.test(getElementValue('#playerName'))) {
        event.target.style.backgroundColor = 'green';
        enableElement(startButton);
    } else {
        event.target.style.backgroundColor = 'red';
        disableElement(startButton);
    }
});

/**
 * @author Alec Morin
 * Event listener that dynamically generates a table when the column/row input is modified
 */
tableSizeOption.addEventListener('input', tableSettingsFinder);

/**
 * Event listener to change colour of all buttons and colour span
 * Changes text to the colour value
 * @author David Parker
 */
colourOptionDiv.addEventListener('change', (event) => {
    const selectedColour = event.target.value;
    const spanColours = document.querySelectorAll('.submitBoxColours');
    const backgroundColours = document.querySelectorAll('.stats-buttons, #submitBoxFirstColour, #submitScoreButton, #startPuzzleButton');
    spanColours.forEach((element) => element.textContent = selectedColour);
    backgroundColours.forEach((element) => element.style.backgroundColor = selectedColour);
});

/**
 * Takes list of tds to determine how many of a specific colour there are
 * @param colour colour to determine amount of
 * @param tdList list of tds to search through table
 * @returns the amount of the colour in the list
 */
function determineColourAmount(colour, tdList) {
    let mainColour = 0;
    tdList.forEach((element) => {
        const tdColour = getHighestRGBValue(element.style.background);
        if (tdColour === colour) {
            mainColour++;
        }
    });
    return mainColour;
}

function changeParagraphNumber() {
    const span = document.querySelector('#submitBoxNum');
    const colourValue = getElementValue('input[name=colourGameOption]:checked');
    const tds = document.querySelectorAll('#game-table tbody tr td');
    span.textContent = determineColourAmount(colourValue, tds);
}

/**
 * @author Alec Morin
 * Event listener that generates a random for each cell in the table
 */
gameDifficulty.addEventListener('input', tableSettingsFinder);

/**
 * Enables and disables cheats during the game
 * @author David Parker
 */
cheatsOptions.addEventListener('change', () => {
    const spans = document.querySelectorAll('td span');
    if (cheatsOptions.checked) {
        showCheats(spans);
    } else {
        hideCheats(spans);
    }
});

/**
 * Turns visibility of the elements to visisble
 * @param spanElements element to hide
 * @author David Parker
 */
function showCheats(spanElements) {
    spanElements.forEach((element) => {
        element.style.visibility = 'visible';
    });
}

/**
 * Turns visibility of the elements to hidden
 * @param spanElements element to hide
 * @author David Parker
 */
function hideCheats(spanElements) {
    spanElements.forEach((element) => {
        element.style.visibility = 'hidden';
    });
}

/**
 * Enables the cheats checkbox
 * @author David Parker
 */
function enableCheats() {
    const cheatOption = document.querySelector('#cheatOption');
    enableElement(cheatOption);
}

/**
 * Disables the cheats checkbox
 * @author David Parker
 */
function disableCheats() {
    const cheatOption = document.querySelector('#cheatOption');
    disableElement(cheatOption);
}

/**
 * Event on startPuzzleButton that disables all options and enables cheats checkbox
 * @author David Parker
 */
startPuzzleButton.addEventListener('click', (event) => {
    const allColourOptions = document.querySelectorAll('input[name=colourGameOption]');
    const options = [[nameOption, tableSizeOption, gameDifficulty, event.target], allColourOptions];
    for (const option of options) {
        option.forEach((element) => {
            disableElement(element);
        });
    }
    hideCheats(document.querySelectorAll('td span'));
    enableCheats();
    tableSettingsFinder();
    enableElement(document.querySelector('#submitScoreButton'));
    changeParagraphNumber();
    stopTimer = timer(document.querySelector('#timer p'));
    recordNumClicks();
});
