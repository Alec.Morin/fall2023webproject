'use strict';

let gameSessions = [];
let stopTimer;

/**
 * @author David Parker
 * @param id element id
 * @returns value of selected id
 */
function getElementValue(id) {
    return document.querySelector(id).value;
}

/**
 * Changes the input disable property to false
 * @param inputElement element of type input
 * @author David Parker
 */
function enableElement(inputElement) {
  inputElement.disabled = false;
}

/**
 * Changes the input disable property to true
 * @param inputElement element of type input
 * @author David Parker
 */
function disableElement(inputElement) {
  inputElement.disabled = true;
}

/**
 * @author Alec Morin
 * @param rowCount
 * @param parent
 * Is used to dynamically build a table with x amount of rows/columns
 *  when called
 */
function generateTable(rowCount, parent, range) {
  parent.innerHTML = '';
  const my_table = document.createElement('table');
  my_table.id = 'game-table';
  const table_body = document.createElement('tbody');
  for (let i = 0; i < rowCount; i++) {
    const my_row = document.createElement('tr');
    for (let x = 0; x < rowCount; x++) {
      const my_cell = document.createElement('td');
      const colorValues = getRandomColorWithRange(range, getElementValue('#gameDifficulty'));
      const rgb = `rgb(${colorValues[0]}, ${colorValues[1]}, ${colorValues[2]})`;
      my_cell.style.background = rgb;
      my_row.appendChild(my_cell);
      const spanElement = document.createElement('span');
      spanElement.style.visibility = 'hidden';
      spanElement.style.background = 'white';
      spanElement.textContent = rgb;
      my_cell.appendChild(spanElement);
    }
    table_body.appendChild(my_row);
  }
  my_table.appendChild(table_body);
  parent.appendChild(my_table);
}

/**
 * Finds the table settings and calls generateTable to generate a table
 * @author Alec Morin
 */
function tableSettingsFinder() {
  const range = determineRange(getElementValue('#gameDifficulty'));
  const gameTable = document.querySelector('#game-table-section');
  generateTable(getElementValue('#tableSizeOption'), gameTable, range);
}

/**
 * @author Alec Morin David Parker
 * @param {Array} rgb
 * determines which of the red, green and blue is highest in a color
 */
function getHighestRGBValue(rgb) {
  const values = rgb.match(/\d+/g).map(Number);
  const red = values[0];
  const green = values[1];
  const blue = values[2];

  let highestColor;
  if (red >= green && red >= blue) {
    highestColor = 'red';
  }
  if (green >= red && green >= blue) {
    highestColor = 'green';
  }
  if (blue >= red && blue >= green) {
    highestColor = 'blue';
  }
  return highestColor;
}

/**
 * @author Alec Morin
 * @param difficulty
 * @returns range
 * helper method used to minize code in gameSettings.js
 * and return the range determined by the difficulty
 */
function determineRange(difficulty) {
  let range;

  if (difficulty === 'medium') {
    range = 80;
  }
  if (difficulty === 'difficult') {
    range = 50;
  }
  if (difficulty === 'brutal') {
    range = 10;
  }
  return range;
}

/**
 * Adds a row as a child of the parent
 * @param parent parent of the table row
 * @author David Parker
 * @returns The row created
 */
function addTableRow(parent) {
  const row = document.createElement('tr');
  parent.append(row);
  return row;
}

/**
 * Adds table data to the parent
 * @param columnCount Amount of columns to add
 * @param parent Parent of the data
 * @param tableData Array of data. Must be the same length ass columnCount
 * @author David Parker
 */
function addTableData(columnCount, parent, tableData) {
  if (tableData.length === columnCount) {
    for (let index = 0; index < columnCount; index++) {
      const column = document.createElement('td');
      column.textContent = tableData[index];
      parent.append(column);
    }
  }
}

/**
 * Clears the table data of the table body
 * @param tableBody
 * @author David Parker
 */
function clearTableData(tableBody) {
  if (tableBody) {
    while (tableBody.firstChild) {
      tableBody.removeChild(tableBody.firstChild);
    }
  }
}

/**
 * @author Alec Morin
 * @param time timer element
 * makes a timer at a selected place in the html
 */
function timer(time) {
  let totalSeconds = 0;
  const interval = setInterval(() => {
    const mins = Math.floor(totalSeconds / 60);
    const secs = totalSeconds % 60;
    if (secs < 10) {
      time.textContent = (`${mins}:0${secs}`);
    } else {
      time.textContent = (`${mins}:${secs}`);
    }
    totalSeconds++;
  }, 1000);

  return function stopTimer() {
    clearInterval(interval);
    return totalSeconds;
  };
}
