'use strict';

const submitGuessesButton = document.querySelector('#submitScoreButton');
const gameTableSection = document.querySelector('#game-table-section');
let clicks = 0;

/**
 * Event listener on submit guesses button
 * Enables gameSettings
 * Disables submit guesses button
 * @author David Parker
 */
submitGuessesButton.addEventListener('click', (event) => {
    const form = document.querySelector('form');
    const allColourOptions = document.querySelectorAll('input[name=colourGameOption]');
    const options = [[nameOption, tableSizeOption, gameDifficulty], allColourOptions];
    for (const option of options) {
        option.forEach((element) => {
            enableElement(element);
        });
    }
    showCheats(document.querySelectorAll('td span'));
    disableCheats();
    disableElement(event.target);
    const totalSeconds = stopTimer();
    const percentage = winPercentage();
    pushGameSettings(totalSeconds, clicks, percentage);
    const tableBody = document.querySelector('#statistics-table tbody');
    addResultsToScoreboard(tableBody, gameSessions[gameSessions.length - 1]);
    form.reset();
});

/**
 * @author Alec Morin
 * @param range
 * @returns a random RGB value with each value within a range
 */
function getRandomColorWithRange(range, difficulty) {
  let red;
  let green;
  let blue;
  if (difficulty === 'easy') {
      red = Math.max(0, Math.floor(Math.random() * 255));
      green = Math.max(0, Math.floor(Math.random() * 255));
      blue = Math.max(0, Math.floor(Math.random() * 255));
  } else {
      range = Math.max(0, Math.min(range, 255));
      red = Math.max(0, Math.floor(Math.random() * (255 - range)));
      green = Math.max(0, Math.floor(Math.random() * ((red + range) - (red - range)) + (red - range)));
      blue = Math.max(0, Math.floor(Math.random() * ((red + range) - (red - range)) + (red - range)));
  }
  return [red, green, blue];
}

/**
 *  @author Alec Morin
 * changes the selected cell's border to red
 * and black
 */
gameTableSection.addEventListener('click', (event) => {
    if (event.target.tagName === 'TD') {
        if (event.target.style.border !== '4px solid red') {
            event.target.style.border = '4px solid red';
        } else {
            event.target.style.border = '1px solid black';
        }
    }
});

/**
 * @author Alec Morin
 * @returns the cells with red borders in an array
 */
function collectCellsGuessed(tdArray) {
    const redCells = [];

    for (let i = 0; i < tdArray.length; i++) {
        if (tdArray[i].style.border === '4px solid red') {
            redCells.push(tdArray[i]);
        }
    }
    return redCells;
}

/**
 * @author Alec Morin
 * @returns the right guesses coresponding to chosen colors
 */
function getRightGuesses(chosenColor, tdArray) {
    const redCells = collectCellsGuessed(tdArray);
    let rightGuesses = 0;

    for (let j = 0; j < redCells.length; j++) {
        if (getHighestRGBValue(redCells[j].style.background) === chosenColor) {
            rightGuesses++;
        }
    }
    return rightGuesses;
}

/**
 * @author Alec Morin
 * @param {Array} tdArray
 * @param chosenColor
 * @return percentage
 * compare amount of rightGuesses compared to number chosen color cells in table
 * to return percentage of success
 */
function winPercentage() {
    const chosenColor = getElementValue('input[name=colourGameOption]:checked');
    const tdArray = document.querySelectorAll('#game-table tbody tr td');
    const cellsGuessed = collectCellsGuessed(tdArray).length;
    const rightGuesses = getRightGuesses(chosenColor, tdArray);
    const amountToGuess = determineColourAmount(chosenColor, tdArray);
    let percentage;

    if (cellsGuessed > amountToGuess) {
        percentage = Math.max(0, Math.floor(100 * ((rightGuesses - (cellsGuessed - rightGuesses)) / amountToGuess)));
    } else {
        percentage = Math.max(0, Math.floor(100 * (rightGuesses / amountToGuess)));
    }
    return percentage;
}

/**
 * @author Alec Morin
 * @returns the amount of clicks on table
 */
function recordNumClicks() {
    clicks = 0;
    const tbody = document.querySelector('#game-table tbody');
    tbody.addEventListener('click', (event) => {
        if (event.target.tagName === 'TD') {
            clicks++;
        }
    });
}
