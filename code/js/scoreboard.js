'use strict';

const saveGameButton = document.querySelector('#save-game-sessions');
const loadGameButton = document.querySelector('#load-game-sessions');
const clearButton = document.querySelector('#clear-game-sessions');
const playerHeader = document.querySelector('#playerHeader');
const durationHeader = document.querySelector('#durationHeader');
const clicksHeader = document.querySelector('#clicksHeader');
const levelHeader = document.querySelector('#levelHeader');
const successHeader = document.querySelector('#successHeader');
const sizeHeader = document.querySelector('#sizeHeader');
const sortingStates = {
    name: 'ascending',
    duration: 'ascending',
    clicks: 'ascending',
    level: 'ascending',
    success: 'ascending',
    size: 'ascending',
  };

/**
 * Saves the gameSessions array to local storage
 * @author David Parker
 */
function saveGameSessions() {
    const serializedSessions = JSON.stringify(gameSessions);
    localStorage.setItem('gameSessions', serializedSessions);
}

/**
 * Loads all gameSessions and adds them to gameSessions array in local JS
 * Adds gameSessions to table
 * @author David Parker
 */
function loadGameSessions() {
    const loadMerge = createLoadMerge();
    if (loadMerge.length - gameSessions.length !== 0) {
        gameSessions = loadMerge;
        generateStatsTable(gameSessions);
    }
}

/**
 * Combines the saves of local storage and gameSessions
 * @returns {Array} returns an array of combined saves
 * @author David Parker
 */
function createLoadMerge() {
    let loadMerge = [];
    const localSaves = JSON.parse(localStorage.getItem('gameSessions'));
    if (localSaves === null) {
        loadMerge = gameSessions;
        return loadMerge;
    }
    loadMerge = gameSessions.concat(localSaves);
    return loadMerge;
}

/**
 * Clears gameSessions local storage
 * @author David Parker
 */
function clearGameSessions() {
    localStorage.removeItem('gameSessions');
}

/**
 * Adds the object fields to the table body
 * @param {Element} tableBody tbody element
 * @param {Object} session game sessions to use
 * @author David Parker
 */
function addResultsToScoreboard(tableBody, session) {
    const newRow = addTableRow(tableBody);
    const valuesArray = Object.values(session);
    addTableData(valuesArray.length, newRow, valuesArray);
}

/**
 * Generates the statistics table
 * @param {Array} sessions
 * @author David Parker
 */
function generateStatsTable(sessions) {
    const tbody = document.querySelector('#statistics-table tbody');
    clearTableData(tbody);
    sessions.forEach((session) => {
        addResultsToScoreboard(tbody, session);
    });
}

/**
 * Changes the sorting order of a certain header
 * @param {String} objectHeaderName
 */
function toggleSorting(objectHeaderName) {
    sortingStates[objectHeaderName] = sortingStates[objectHeaderName] === 'ascending' ? 'descending' : 'ascending';
}

/**
 * Sorts the array by alphabetical order
 * @param {Array} array Array of gameSession objects
 * @author David Parker
 */
function sortByStringInsensitive(array) {
    if (sortingStates.name === 'ascending') {
        array.sort((a, b) => a.name.localeCompare(b.name, undefined, { sensitivity: 'base' }));
    } else {
        array.sort((a, b) => b.name.localeCompare(a.name, undefined, { sensitivity: 'base' }));
    }
}

/**
 * Sorts the array in ascending order
 * @param {Array} array Array of gameSession objects
 * @author David Parker
 */
function sortByDuration(array) {
    if (sortingStates.duration === 'ascending') {
        array.sort((a, b) => a.duration - b.duration);
    } else {
        array.sort((a, b) => b.duration - a.duration);
    }
}

/**
 * Sorts the array in ascending order
 * @param {Array} array Array of gameSession objects
 * @author David Parker
 */
function sortByClicks(array) {
    if (sortingStates.clicks === 'ascending') {
        array.sort((a, b) => a.clicks - b.clicks);
    } else {
        array.sort((a, b) => b.clicks - a.clicks);
    }
}

/**
 * Sorts the array according to level
 * @param {Array} array Array of gameSession objects
 */
function sortByLevel(array) {
    const difficultyOrder = {
        'easy': 1,
        'medium': 2,
        'difficult': 3,
        'brutal': 4,
    };
    if (sortingStates.level === 'ascending') {
        array.sort((a, b) => difficultyOrder[a.difficulty] - difficultyOrder[b.difficulty]);
    } else {
        array.sort((a, b) => difficultyOrder[b.difficulty] - difficultyOrder[a.difficulty]);
    }
}

/**
 * Sorts the array in ascending order
 * @param {Array} array Array of gameSession objects
 * @author David Parker
 */
function sortBySuccess(array) {
    if (sortingStates.success === 'ascending') {
        array.sort((a, b) => a.success - b.success);
    } else {
        array.sort((a, b) => b.success - a.success);
    }
}

/**
 * Sorts the array in ascending order
 * @param {Array} array Array of gameSession objects
 * @author David Parker
 */
function sortBySize(array) {
    if (sortingStates.size === 'ascending') {
        array.sort((a, b) => a.size - b.size);
    } else {
        array.sort((a, b) => b.size - a.size);
    }
}

// Event listeners
saveGameButton.addEventListener('click', () => {
    if (window.confirm('Warning: If you proceed, your local data will be permanently deleted. Make sure to load your existing saves before continuing. Do you wish to proceed?')) {
        saveGameSessions();
    }
});
loadGameButton.addEventListener('click', loadGameSessions);
clearButton.addEventListener('click', () => {
    if (window.confirm('Are you sure you want to clear all your game sessions?')) {
        clearGameSessions();
    }
});
playerHeader.addEventListener('click', () => {
    toggleSorting('name');
    sortByStringInsensitive(gameSessions);
    generateStatsTable(gameSessions);
});
durationHeader.addEventListener('click', () => {
    toggleSorting('duration');
    sortByDuration(gameSessions);
    generateStatsTable(gameSessions);
});
clicksHeader.addEventListener('click', () => {
    toggleSorting('clicks');
    sortByClicks(gameSessions);
    generateStatsTable(gameSessions);
});
levelHeader.addEventListener('click', () => {
    toggleSorting('level');
    sortByLevel(gameSessions);
    generateStatsTable(gameSessions);
});
successHeader.addEventListener('click', () => {
    toggleSorting('success');
    sortBySuccess(gameSessions);
    generateStatsTable(gameSessions);
});
sizeHeader.addEventListener('click', () => {
    toggleSorting('size');
    sortBySize(gameSessions);
    generateStatsTable(gameSessions);
});
